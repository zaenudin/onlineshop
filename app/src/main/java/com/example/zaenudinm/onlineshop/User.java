package com.example.zaenudinm.onlineshop;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class User extends AppCompatActivity {

    EditText nama,nohp,alamat;
    Button btn_send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        getSupportActionBar().setTitle("Data User");
        getSupportActionBar().setSubtitle("Sending Data");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nama = (EditText) findViewById(R.id.edt_name_user);
        nohp = (EditText) findViewById(R.id.edt_noHp_user);
        alamat = (EditText) findViewById(R.id.edt_alamat_user);
        btn_send = (Button) findViewById(R.id.Btn_send_data);

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nama.getText().toString().equals("")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(User.this);
                    builder.setMessage("name not valid").setNegativeButton("retry",null).create().show();
                }else if (nohp.getText().toString().equals("")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(User.this);
                    builder.setMessage("phone number not valid").setNegativeButton("retry",null).create().show();
                }else if (alamat.getText().toString().equals("")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(User.this);
                    builder.setMessage("address not valid").setNegativeButton("retry",null).create().show();
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(User.this);
                    builder.setMessage("Send Data, Sukses").setNegativeButton("OK",null).create().show();
                    nama.setText("");
                    nama.requestFocus();
                    nohp.setText("");
                    nohp.requestFocus();
                    alamat.setText("");
                    alamat.requestFocus();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = (item.getItemId());
        if (item.getItemId()==R.id.inbox){
            startActivity(new Intent(User.this,Inbox.class));

        }else if (item.getItemId()==R.id.logout){
            startActivity(new Intent(User.this,Login.class));
        }else{
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
