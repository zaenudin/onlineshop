package com.example.zaenudinm.onlineshop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailData extends AppCompatActivity {

    TextView dtlnama,dtlharga;
    ImageView dtlimages;
    Button btndtl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_data);

        getSupportActionBar().setTitle("Detail data");
        getSupportActionBar().setSubtitle("Order Item");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dtlnama = (TextView)findViewById(R.id.dtl_nama);
        dtlharga = (TextView)findViewById(R.id.dtl_harga);
        dtlimages = (ImageView)findViewById(R.id.dtl_images);
        btndtl = (Button)findViewById(R.id.btn_dtlData);

        final Intent intent = getIntent();
        dtlnama.setText(intent.getStringExtra("nama"));
        dtlharga.setText(intent.getStringExtra("harga"));
        dtlimages.setImageResource(intent.getIntExtra("image",0));

        btndtl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent move = new Intent(DetailData.this,User.class);
                startActivity(move);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (item.getItemId()==R.id.inbox){
            startActivity(new Intent(DetailData.this,Inbox.class));
        }else if (item.getItemId()==R.id.logout){
            startActivity(new Intent(this,Login.class));
        }else if (id ==android.R.id.home){
            startActivity(new Intent(this,ListItem.class));
        }

        return true;
    }
}
