package com.example.zaenudinm.onlineshop;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Loginresponsi extends AppCompatActivity {

    EditText email, password;
    TextView regis,lupa;
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginresponsi);

        email = (EditText) findViewById(R.id.edt_email);
        password = (EditText) findViewById(R.id.edt_pass);
        regis = (TextView) findViewById(R.id.register_btn);
        login = (Button) findViewById(R.id.btn_sigin);
        lupa = (TextView) findViewById(R.id.forgetpass);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailkey = email.getText().toString();
                String passwordkey = password.getText().toString();

                if (emailkey.equals("member@gmail.com") && passwordkey.equals("abc123")) {

                    Intent intent = new Intent(Loginresponsi.this, ListItem.class);
                    Loginresponsi.this.startActivity(intent);
                    finish();
                    email.setText("");
                    email.requestFocus();
                    password.setText("");

                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(Loginresponsi.this);
                    builder.setMessage("Email & Password Anda Salah").setNegativeButton("coba lagi", null).create().show();
                    password.setText("");
                }

            }
        });
        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Loginresponsi.this);
                builder.setMessage("SIPOKE").setNegativeButton("OK", null).create().show();
            }
        });
        lupa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Loginresponsi.this);
                builder.setMessage("SIPOKE").setNegativeButton("OK", null).create().show();


            }
        });
    }




}
