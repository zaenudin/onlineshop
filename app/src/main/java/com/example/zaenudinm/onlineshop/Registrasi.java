package com.example.zaenudinm.onlineshop;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Registrasi extends AppCompatActivity {

    EditText email,pass,pass2;
    Button btnRegis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        getSupportActionBar().setTitle("Online Shope");
        getSupportActionBar().setSubtitle("Registrasi Akun");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        email = (EditText) findViewById(R.id.regis_email);
        pass = (EditText) findViewById(R.id.regis_pass);
        pass2 = (EditText) findViewById(R.id.regis_pass2);
        btnRegis = (Button) findViewById(R.id.btn_regis);

        btnRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email.getText().toString().equals("")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(Registrasi.this);
                    builder.setMessage("Email Not Valid").setNegativeButton("Retry",null).create().show();
                }else if (pass.getText().toString().equals("")){
                    AlertDialog.Builder builder =  new AlertDialog.Builder(Registrasi.this);
                    builder.setMessage("Password Not Valid ").setNegativeButton("Retry",null).create().show();
                }else if (pass2.getText().toString().equals("")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(Registrasi.this);
                    builder.setMessage("Password tidak cocok").setNegativeButton("Retry",null).create().show();
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(Registrasi.this);
                    builder.setMessage("Sukses Create Akun").setNegativeButton("Ok",null).create().show();
                    email.setText("");
                    email.requestFocus();
                    pass.setText("");
                    pass.requestFocus();
                    pass2.setText("");
                    pass2.requestFocus();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = (item.getItemId());
        if (id==android.R.id.home){
            finish();
        }

        return true;
    }
}
