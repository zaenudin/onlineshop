package com.example.zaenudinm.onlineshop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;

public class ListItem extends AppCompatActivity {

    String nama[]=new String[]{"GIORDANO WHITE","KIDROCK ABU","NIKE ABU","OBLONG ABU GELAP","ORGINAL BLACK","SWITTER WHITE"};
    String harga[]=new String[]{"50.000","60.000","70.000","50.000","50.000","50.000"};
    int images[]={R.drawable.giordano_white,R.drawable.kidrock_abu,R.drawable.nike_abu,R.drawable.oblong_abu,R.drawable.original_black,R.drawable.switter_white};

    ListView listView;

    Pesan pesan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_item);

        getSupportActionBar().setTitle("List Item");
        getSupportActionBar().setSubtitle("All Item");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pesan = new Pesan(ListItem.this,nama,harga,images);
        listView = (ListView)findViewById(R.id.item_list);
        listView.setAdapter(pesan);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (item.getItemId()==R.id.inbox){

        }else if (item.getItemId()==R.id.logout){
            startActivity(new Intent(this,Login.class));
        }else if (id==android.R.id.home){
            startActivity(new Intent(this,Login.class));
            finish();
        };

        return true;
    }
}
