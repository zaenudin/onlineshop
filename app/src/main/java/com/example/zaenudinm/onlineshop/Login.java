package com.example.zaenudinm.onlineshop;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Login extends AppCompatActivity {

    EditText email,password;
    TextView regis;
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().setTitle("Online Shope");
        getSupportActionBar().setSubtitle("Login Akun");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        email = (EditText)findViewById(R.id.edt_email);
        password = (EditText)findViewById(R.id.edt_pass);
        regis = (TextView)findViewById(R.id.register_btn);
        login = (Button)findViewById(R.id.btn_sigin);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailkey = email.getText().toString();
                String passwordkey = password.getText().toString();

                if (emailkey.equals("member@gmail.com")&& passwordkey.equals("abc123")){

                    Intent intent = new Intent(Login.this,ListItem.class);
                    Login.this.startActivity(intent);
                    finish();
                    email.setText("");
                    email.requestFocus();
                    password.setText("");

                }else{

                    AlertDialog.Builder builder =  new AlertDialog.Builder(Login.this);
                    builder.setMessage("Email & Password Anda Salah").setNegativeButton("coba lagi",null).create().show();
                    password.setText("");
                }

            }
        });
        regis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this,Registrasi.class);
                Login.this.startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home){
            //finish();
            moveTaskToBack(true);
        }
        return true;
    }
}
