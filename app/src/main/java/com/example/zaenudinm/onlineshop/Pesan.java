package com.example.zaenudinm.onlineshop;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by zaenudinm on 3/30/2018.
 */

public class Pesan extends ArrayAdapter<String> {

    String arrNama[],arrHarga[];
    int arrImage[];

    Context c;

    LayoutInflater inflater;

    public Pesan(Context context, String[] nama, String[] harga, int[] images) {
        super(context,R.layout.inflate_list,nama);
        this.arrNama = nama;
        this.arrHarga = harga;
        this.arrImage = images;
        this.c = context;
    }

    public class ViewList{
        TextView VL_nama,VL_harga;
        ImageView VL_images;
        LinearLayout linearLayout;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {

        if (convertView==null){

            inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.inflate_list,null);
        }
        final ViewList list = new ViewList();
        list.VL_nama = (TextView)convertView.findViewById(R.id.if_nama);
        list.VL_harga = (TextView)convertView.findViewById(R.id.if_harga);
        list.VL_images = (ImageView)convertView.findViewById(R.id.if_image);
        list.linearLayout = (LinearLayout) convertView.findViewById(R.id.if_list);

        list.VL_nama.setText(arrNama[position]);
        list.VL_harga.setText("Rp"+ arrHarga[position]);
        list.VL_images.setImageResource(arrImage[position]);

        list.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent move = new Intent(parent.getContext(),DetailData.class);
                move.putExtra("nama",arrNama[position]);
                move.putExtra("harga",arrHarga[position]);
                move.putExtra("image",arrImage[position]);
                parent.getContext().startActivity(move);

            }
        });

        return convertView;
    }
}
